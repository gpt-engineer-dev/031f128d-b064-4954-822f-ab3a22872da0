document.querySelector("button").addEventListener("click", addTodo);

function addTodo() {
  const input = document.querySelector("input");
  if (input.value != "") {
    addTodoElement(input.value);
    input.value = "";
  }
}

function addTodoElement(text) {
  const todoDiv = document.createElement("div");
  todoDiv.classList.add("flex", "mb-4", "items-center");

  const p = document.createElement("p");
  p.classList.add("w-full", "text-grey-darkest");
  p.textContent = text;
  todoDiv.appendChild(p);

  const doneButton = document.createElement("button");
  doneButton.classList.add(
    "flex-no-shrink",
    "p-2",
    "ml-4",
    "mr-2",
    "border-2",
    "rounded",
    "hover:text-white",
    "text-green",
    "border-green",
    "hover:bg-green"
  );
  doneButton.textContent = "Done";
  doneButton.addEventListener("click", markAsDone);
  todoDiv.appendChild(doneButton);

  const removeButton = document.createElement("button");
  removeButton.classList.add(
    "flex-no-shrink",
    "p-2",
    "ml-2",
    "border-2",
    "rounded",
    "text-red",
    "border-red",
    "hover:text-white",
    "hover:bg-red"
  );
  removeButton.textContent = "Remove";
  removeButton.addEventListener("click", remove);
  todoDiv.appendChild(removeButton);

  document.querySelector(".container").appendChild(todoDiv);
}

function markAsDone(event) {
  event.target.parentNode.firstChild.classList.add("line-through");
}

function remove(event) {
  event.target.parentNode.remove();
}
